﻿using UnityEngine;
using System.Collections;

namespace GLOBAL_SETTINGS {
	class CURRENT
	{
		public static float score = 0;

		public static bool shouldThrowFood = true;

		public static bool shouldStartDeathZoneTimer = false;

		public static bool shouldStartWinZoneTimer = false;
	}

	class SETTINGS
	{
		public static float speedFood = 6;
		// Range of the goal
		public static int rangeGoal = 3;
		// Range of the point
		public static int rangePoint = 20;
		// HugeAttacks activated
		public static bool hugeAttacks = true;
		// Range of the goal
		public static int rangeFood = 5;

		public static float timeInSecondsBetweenTwoFoods = 3;

		public static float playTime = 180;
		public static float timeToStayInArea = 18;
	}

	class SCENE
	{
		// Z axis end position of the food
		public static float endPointOfFood = -10;
	}
}
