﻿using UnityEngine;
using System.Collections;

public class WinTimer : MonoBehaviour {
	private float startTime;
	private bool shouldUpdateGUI;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if((Time.time > (startTime + GLOBAL_SETTINGS.SETTINGS.timeToStayInArea)) /*&& (GLOBAL_SETTINGS.CURRENT.shouldStartWinZoneTimer)*/)
		{
			Debug.Log("YYYEAAAAHHHHHhhhhhhh...... ");
			shouldUpdateGUI = false;
			GLOBAL_SETTINGS.CURRENT.shouldThrowFood = false;
		}
	}
	
	void OnGUI()
	{
		if(shouldUpdateGUI)
		{
			float ellapsedTime = startTime + GLOBAL_SETTINGS.SETTINGS.timeToStayInArea - Time.time;
			int roundedEllapsedTime = Mathf.CeilToInt(ellapsedTime);
			int seconds = (int) roundedEllapsedTime % 60;
			//int minutes = (int) roundedEllapsedTime / 60;
			
			//GUI.Label(new Rect(10, 10, 100, 20), minutes.ToString() + ":" + seconds.ToString());
			GUI.Label(new Rect(50, 50, 100, 20), string.Format("{0:00}", seconds));
		}
	}
	
	public void resetTimer()
	{
		startTime = Time.time;
	}
}
