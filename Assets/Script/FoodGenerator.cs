﻿using UnityEngine;
using System.Collections;

public class FoodGenerator : MonoBehaviour {

	private int foodFactor;

	public int getFoodFactor()
	{
		return foodFactor;
	}

	// Use this for initialization
	void Start () {
		foodFactor = Random.Range( - GLOBAL_SETTINGS.SETTINGS.rangeFood, GLOBAL_SETTINGS.SETTINGS.rangeFood);

		if(foodFactor == 0)
		{
			//Set path to fourchette
			this.renderer.material.color = Color.grey;
		}
		else if(foodFactor < 0)
		{
			//Set path to Fruit
			this.renderer.material.color = Color.green;
			setFoodSize();
		}
		else
		{
			//Set path to Burger
			this.renderer.material.color = Color.red;
			setFoodSize();
		}
	}

	void setFoodSize()
	{
		this.transform.localScale = Vector3.one * (( Mathf.Abs((float) foodFactor) / GLOBAL_SETTINGS.SETTINGS.rangeFood));
	}
}
