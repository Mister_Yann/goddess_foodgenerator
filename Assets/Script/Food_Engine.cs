﻿using UnityEngine;
using System.Collections;

public class Food_Engine : MonoBehaviour {
	//Paramètres de gestion
	//Position de depart
	private Vector3 startPosition = Vector3.zero;
	// Angle de courbure de la trajectoire
	private Vector3 bending = Vector3.up;

	private float timeToTravel = GLOBAL_SETTINGS.SETTINGS.speedFood;
	// Distance en Z de fin de course de l'objet
	private float endDistance = GLOBAL_SETTINGS.SCENE.endPointOfFood * 2;
	// Distance en Z de destruction de l'objet
	private float deadZone = GLOBAL_SETTINGS.SCENE.endPointOfFood * 1.5f;
	// Rayon de tir
	private float rayon = 3;
	// Borne de tir
	public float minAngleDeTir = 0;
	public float maxAngleDeTir = Mathf.PI;
	
	void setRayon(float newRayon){
		rayon = newRayon;
	}
	
	void setEndPosition(float newEndPosition){
		endDistance = newEndPosition * 2;
		deadZone = newEndPosition * 1.5f ;
	}

	void setTimeToTravel(float newTimeToTravel)
	{
		timeToTravel = newTimeToTravel;
	}

	// Use this for initialization
	void Update() {
		if(transform.position.z <= deadZone){
			Destroy(this.gameObject);
			Destroy(this);
		}
	}
	
	/**
	 * Bouge l'objet sur une trajectoire en elipse sur un angle aléatoire
	 */
	void MoveToRandomPosition(){
		float angle = Random.Range(minAngleDeTir,maxAngleDeTir);
		StartCoroutine(MoveToPosition(angle));
	}
	
	/**
	 * Bouge l'objet sur une trajectoire en elipse sur l'angle défini
	 * 
	 * 	Parametre :
	 * 		- angle : angle de destination
	 */
	void MoveToDefinedPosition(float angle){
		StartCoroutine(MoveToPosition(angle));
	}
	
	/*
	 * Bouge l'objet vers la position définit par x et y en un mouvement d'elipse
	 * 
	 * Parametres : 
	 * 		- x : position x de destination
	 * 		- y : position y de destination
	 */
	private IEnumerator MoveToPosition(float angle) {
		float x = rayon * Mathf.Cos(angle);
		float y = rayon * Mathf.Sin(angle);
		Vector3 endPosition = new Vector3(x,y,endDistance);
		float timeStamp = Time.time;
		while (Time.time<timeStamp+timeToTravel) {
			Vector3 currentPos = Vector3.Lerp(startPosition, endPosition, (Time.time - timeStamp)/timeToTravel);
			
			currentPos.x += bending.x*Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp)/timeToTravel) * Mathf.PI * 2);
			currentPos.y += bending.y*Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp)/timeToTravel) * Mathf.PI * 2);
			currentPos.z += bending.z*Mathf.Sin(Mathf.Clamp01((Time.time - timeStamp)/timeToTravel) * Mathf.PI * 2);

			this.transform.position = currentPos;
			
			yield return null;
		}
	}
}
