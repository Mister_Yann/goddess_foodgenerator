﻿using UnityEngine;
using System.Collections;

public class Player_Manager : MonoBehaviour {
	public GameObject bassin;
	public GameObject head;

	void Update()
	{
		// Penchement de cote (inversion des angles car la camera se situe deriere le joueur)
		float angleZPlan = -Vector2.Angle(Vector2.right, new Vector2(head.transform.position.x-bassin.transform.position.x,head.transform.position.y-bassin.transform.position.y));
		// Penchement en avant (inversion des angles car la camera se situe deriere le joueur)
		float angleXPlan = -Vector2.Angle(Vector2.right, new Vector2(head.transform.position.z-bassin.transform.position.z,head.transform.position.y-bassin.transform.position.y));
		
		// Conpensation des
		this.transform.parent.gameObject.transform.rotation = Quaternion.Euler(angleXPlan-90,0,angleZPlan-90);
	}
	
	void OnCollisionEnter(Collision _col)
	{
		Debug.Log("Hit");
		
		if(_col.gameObject.tag == "Food")
		{
			GLOBAL_SETTINGS.CURRENT.score += _col.gameObject.GetComponent<FoodGenerator>().getFoodFactor();
			if(GLOBAL_SETTINGS.CURRENT.score > GLOBAL_SETTINGS.SETTINGS.rangePoint)
			{
				GLOBAL_SETTINGS.CURRENT.score = GLOBAL_SETTINGS.SETTINGS.rangePoint;
			}
			else if(GLOBAL_SETTINGS.CURRENT.score < - GLOBAL_SETTINGS.SETTINGS.rangePoint)
			{
				GLOBAL_SETTINGS.CURRENT.score = - GLOBAL_SETTINGS.SETTINGS.rangePoint;
			}
			Debug.Log("Score : " + GLOBAL_SETTINGS.CURRENT.score);
			//sceneManager.SendMessage("updateScore",_col.gameObject.GetComponent<FoodFactor>().getValue());
			Destroy(_col.gameObject);
		}
	}
}
