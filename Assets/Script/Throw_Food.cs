﻿using UnityEngine;
using System.Collections;

public class Throw_Food : MonoBehaviour {

	private float lastGeneratedFoodTime = 0;

	// Update is called once per frame
	void Update () {
		if((Time.time >= (lastGeneratedFoodTime + GLOBAL_SETTINGS.SETTINGS.timeInSecondsBetweenTwoFoods))&& GLOBAL_SETTINGS.CURRENT.shouldThrowFood)
		{
			GameObject food = GameObject.CreatePrimitive(PrimitiveType.Cube);
			food.name = "Projectile";
			food.tag="Food";
			food.AddComponent<Food_Engine>();
			food.AddComponent<FoodGenerator>();
			food.AddComponent<Rigidbody>();
			food.rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
			food.SendMessage("MoveToRandomPosition");


			/*float angle = 0;
			for(int x = 0; x < 5; x++)
			{
				GameObject food = GameObject.CreatePrimitive(PrimitiveType.Cube);
				food.name = "Projectile";
				food.tag="Food";
				food.AddComponent<Food_Engine>();
				food.AddComponent<FoodGenerator>();
				food.AddComponent<Rigidbody>();
				food.rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
				food.SendMessage("MoveToDefinedPosition",angle);
				angle += Mathf.PI / 5;
			}*/

			lastGeneratedFoodTime = Time.time;
		}
	}
}
