﻿using UnityEngine;
using System.Collections;

public class ScoreDisplayer : MonoBehaviour {
	private float m_score;

	public int totalAngle = 60;
	
	private int openingScore;
	private int openingAngle;
	
	private int targetScore;
	public int thicknessTargetScore;
	
	// Use this for initialization
	void Start () {
		
		openingScore = GLOBAL_SETTINGS.SETTINGS.rangePoint / 2;
		openingAngle = totalAngle / 2;
		targetScore = Random.Range(-openingScore + thicknessTargetScore, openingScore - thicknessTargetScore);

		//place objective flags here
		GameObject leftGoal = GameObject.Instantiate(Resources.Load("Goal"),Vector3.zero, Quaternion.Euler (new Vector3(0,getAngleFromScore(targetScore + thicknessTargetScore)+180,0))) as GameObject;
		GameObject rightGoal = GameObject.Instantiate(Resources.Load("Goal"),Vector3.zero, Quaternion.Euler (new Vector3(0,getAngleFromScore(targetScore - thicknessTargetScore)+180,0))) as GameObject;

		
	}
	
	// Update is called once per frame
	void Update () {
		updateRotation();
		checkGoal();
	}
	
	//Update background when goal is reached
	private void checkGoal()
	{
		if (((targetScore - thicknessTargetScore) < GLOBAL_SETTINGS.CURRENT.score) && (GLOBAL_SETTINGS.CURRENT.score < (targetScore + thicknessTargetScore)))
		{
			Debug.Log("you're On win ZONE");
		}
		else
		{
			Debug.Log("you're out of win ZONE");
			gameObject.GetComponent<WinTimer>().resetTimer();
		}

		if( (GLOBAL_SETTINGS.CURRENT.score <= -GLOBAL_SETTINGS.SETTINGS.rangePoint) || (GLOBAL_SETTINGS.CURRENT.score >= GLOBAL_SETTINGS.SETTINGS.rangePoint))
		{
			Debug.Log("you're dying");
		}
		else
		{
			Debug.Log("you're good");
			gameObject.GetComponent<DeathTimer>().resetTimer();
		}
	}

	// update the rotation whith smooth rotation
	private void updateRotation()
	{
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, getAngleFromScore(GLOBAL_SETTINGS.CURRENT.score)-180, 0)), Time.deltaTime * 1f);
	}

	// return the angle from griven score
	private float getAngleFromScore(float _score)
	{
		return _score * (openingAngle / openingScore);
	}
}
